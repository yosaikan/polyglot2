.. Polyglot2 documentation master file, created by
   sphinx-quickstart on Thu Apr 10 18:56:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Polyglot2's documentation!
=====================================
Polyglot2 is a package which allows one to build language models that learn distributed representations of words.

Distributed representations of words (very popularly known as *word embeddings*) have been shown to be useful as 
features for several Natural Language Processing Tasks like Named Entity Recognition etc.  

Polyglot2 allows you to create your own embeddings from text. It's a piece of cake really. Try it out !.

.. toctree::
   :hidden:

   intro
   install
   tutorial
   citing
   contact
