.. _install:

==================
Installation guide
==================

Pre-requisites
==============

The installation steps assume that you have the following things installed:

`Python`_ 2.7
---------------

Python 3.0 support is coming soon.

BLAS
----------
Our library requires `cblas`_ headers to compile. Ubuntu comes with two options:

* OpenBLAS: ::

    sudo apt-get install libopenblas-base  libopenblas-dev

* ATLAS: ::

    sudo apt-get install libatlas3gf-base libatlas-dev


The choice of BLAS library influences greatly the speed of the training. We recommend using OpenBLAS as it proved to be faster than ATLAS by **4x**.
In case, you want to compile your own OpenBLAS or switch between the two libraries, we compiled a list of useful commands for that purpose in this Tutorial_.


   
`cython`_
---------------
Cython is optional but recommended. 

Installing Polyglot2 
====================

Download the source from https://bitbucket.org/aboSamoor/polyglot2

Install by using the following command: ::
   
    python setup.py install 


You are all set !

.. _Python: http://www.python.org
.. _cython: http://cython.org/
.. _cblas: http://www.netlib.org/blas/
.. _Tutorial: https://docs.google.com/document/d/1ogv2RXBFyuDDAnB6bmUf2wzp3EYPLHmTF9WK4qt7Lp4/edit?usp=sharing.